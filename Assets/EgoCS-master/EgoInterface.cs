﻿using UnityEngine;
public class EgoInterface : MonoBehaviour
{
	static EgoInterface()
	{
		EgoSystems.Add(

			new InputSystem(),
			new ControllerColliderSystem(),
			new CatchSystem(),
			new RotationSystem(),
			new ConnectPuzzleSystem(),
			new ConnectPuzzleSolutionSystem(),
			new ModulSystem(),
			new ControlModulSystem(),
			new LineRendererSystem(),
			new SphereRotationSystem(),
			new PatternPuzzleSystem(),
			new PatternPuzzleCollisionSystem(),
			new ColorBlinkSystem()
		);
    }

    void Start()
	{
		EgoSystems.Start(); 
	}
	
	void Update()
	{
		EgoSystems.Update();
	}
	
	void FixedUpdate()
	{
		EgoSystems.FixedUpdate();
	}
}
