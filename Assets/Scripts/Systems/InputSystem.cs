﻿using UnityEngine;
using System;

/// <summary>
/// The InputSystem processes the users interaction with the HTC Vive controllers.
/// </summary>
public class InputSystem : EgoSystem < 
	EgoConstraint<Transform, SteamVR_TrackedObject, ControllerComponent, ColliderComponent, SphereCollider, OnCollisionEnterComponent, 
		OnCollisionExitComponent, Rigidbody> 
>{

	public override void Start () {
		
		// EgoEvents<StartRotation>.AddHandler(Handle);

		InitialiseControllers();
	}
	/// <summary>
	/// Checks at every frame if the user press, hold or release the trigger button on the controller and fires a InputEvent with the 
	/// current state as string parameter. 
	/// </summary>
	public override void FixedUpdate () {

		constraint.ForEachGameObject((EgoComponent, Transform, SteamVR_TrackedObject, ControllerComponent, ColliderComponent, 
			SphereCollider, OnCollisionEnterComponent, OnCollisionExitComponent, Rigidbody) => {

			try {
				// Gets the steamVR controller. 
				ControllerComponent.device = SteamVR_Controller.Input((int)SteamVR_TrackedObject.index);
				// Checks if the trigger button is pressed down.
				if (ControllerComponent.device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger)) {
					// Debug.Log("Trigger has been pressed DOWN on device_" + ControllerComponent.device.index);
					var e = new InputEvent("TriggerDown");
					EgoEvents<InputEvent>.AddEvent(e);

					ControllerComponent.device.TriggerHapticPulse(1000);
				}
				// Checks if the trigger button is on hold.
				if (ControllerComponent.device.GetPress(SteamVR_Controller.ButtonMask.Trigger)) {
					// Debug.Log("Trigger is pressed on device_" + ControllerComponent.device.index);
					var e = new InputEvent("TriggerOnHold");
					EgoEvents<InputEvent>.AddEvent(e);
				
				}
				// Checks if the trigger button is released.
				if (ControllerComponent.device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)) {
					// Debug.Log ("Trigger has been pressed UP on device_" + ControllerComponent.device.index);
					var e = new InputEvent("TriggerUp");
					EgoEvents<InputEvent>.AddEvent(e);

					ControllerComponent.device.TriggerHapticPulse(1000);

				}
			}
			// Catches the IndexOutOfRangeExecption until the controllers are recognized by the tracking system.
			catch (IndexOutOfRangeException) {
				// Debug.Log("The controllers have not been recognized yet. Move them to start tracking.");
			}

        });
	}

	/*void Handle(StartRotation e) {

		constraint.ForEachGameObject((EgoComponent, Transform, SteamVR_TrackedObject, ControllerComponent, ColliderComponent, 
			SphereCollider, OnCollisionEnterComponent, OnCollisionExitComponent, Rigidbody) => {

			ControllerComponent.device.TriggerHapticPulse(1000);
        });
	}*/

	/// <summary>
	/// Initialises the controllers entities and their components. 
	/// </summary>
	void InitialiseControllers() {

		var cameraRig = GameObject.FindGameObjectWithTag("cameraRig");

		if (cameraRig == null) {

			Debug.Log("Camera rig not found");
		}

		// Instantiates right controller entity. 
		var controllerRight = Ego.AddGameObject(cameraRig.transform.Find("Controller (right)").gameObject);

		if (controllerRight == null) {

			Debug.Log("Right controller not found");
		}

		var sphereRight = Ego.AddComponent<SphereCollider>(controllerRight);
		var controllerCRight = Ego.AddComponent<ControllerComponent>(controllerRight);
		var rigidBRight = Ego.AddComponent<Rigidbody>(controllerRight);
		var modelRight = controllerRight.transform.Find("Model").gameObject;
		var attachRight = modelRight.transform.Find("tip/attach").gameObject;

		Ego.AddComponent<OnCollisionEnterComponent>(controllerRight);
		Ego.AddComponent<OnCollisionExitComponent>(controllerRight);
		Ego.AddComponent<ColliderComponent>(controllerRight);


		sphereRight.radius = 0.03f;
		rigidBRight.useGravity = false;
		controllerCRight.attachPoint = attachRight.GetComponent<Rigidbody>();

		// Instantiates left controller entity. 
		var controllerLeft = Ego.AddGameObject(cameraRig.transform.Find("Controller (left)").gameObject);

		if (controllerLeft == null) {

			Debug.Log("Left controller not found");
		}

		var sphereLeft = Ego.AddComponent<SphereCollider>(controllerLeft);
		var controllerCLeft = Ego.AddComponent<ControllerComponent>(controllerLeft);
		var rigidBLeft = Ego.AddComponent<Rigidbody>(controllerLeft);
		var modelLeft = controllerLeft.transform.Find("Model").gameObject;
		var attachLeft = modelLeft.transform.Find("tip/attach").gameObject;

		Ego.AddComponent<OnCollisionEnterComponent>(controllerLeft);
		Ego.AddComponent<OnCollisionExitComponent>(controllerLeft);
		Ego.AddComponent<ColliderComponent>(controllerLeft);

		sphereLeft.radius = 0.03f;
		rigidBLeft.useGravity = false;
		controllerCLeft.attachPoint = attachLeft.GetComponent<Rigidbody>();
	}
}
