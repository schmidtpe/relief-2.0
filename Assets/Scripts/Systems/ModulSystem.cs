using UnityEngine;

/// <summary>
/// The ModulSystems enables the activation of the connect puzzle lines and its control moduls to rotate them. 
/// </summary>
public class ModulSystem : EgoSystem < EgoConstraint<Transform, BoxCollider, OnTriggerEnterComponent, OnTriggerExitComponent, 
	ModulComponent> > {

	public override void Start() {

		InstantiateModul();

		// EgoEvents<DestroyObjectOnModulEvent>.AddHandler(Handle);
	}

	/// <summary>
	/// Handles the TriggerEnterEvent received from the OnTriggerEnterComponent to check if a entity marked with 
	/// a ConnectPuzzleCubeComponent entered the modul objects trigger. 
	/// Fires a SphereActEvent to inform other systems which sphere was activated and to activate the control modul.
	/// </summary>
	void Handle(TriggerEnterEvent e) {

		if (e.egoComponent1.HasComponents<ModulComponent>() && 
			e.egoComponent2.HasComponents<ConnectPuzzleCubeComponent>()) {

			Debug.Log(e.egoComponent2.name + " on modul");
			// The color of the object in the collider specifies which sphere should be activated
			e.egoComponent1.GetComponent<ModulComponent>().objectOnModul = e.egoComponent2.gameObject;
			var newEvent = new SphereActEvent(e.egoComponent2.GetComponent<Renderer>().material.color);
			EgoEvents<SphereActEvent>.AddEvent(newEvent);
		}
		else if (e.egoComponent1.HasComponents<ConnectPuzzleCubeComponent>() &&
			e.egoComponent2.HasComponents<ModulComponent>()) {

			Debug.Log(e.egoComponent2.name + " on modul");
			e.egoComponent2.GetComponent<ModulComponent>().objectOnModul = e.egoComponent1.gameObject;
			var newEvent = new SphereActEvent(e.egoComponent1.GetComponent<Renderer>().material.color);
			EgoEvents<SphereActEvent>.AddEvent(newEvent);
		}

		// optional: collider is exactly in the triggers center 

		/*if (modulCollider !=null && modulCollider.bounds.Contains(e.collider.bounds.min) && modulCollider.bounds.Contains(e.collider.bounds.max)) {

			//Debug.Log("modul is containing " + e.collider.name +  " in the center of its collider");

			var newEvent = new SphereActEvent(e.collider.GetComponent<Renderer>().material.color);
			EgoEvents<SphereActEvent>.AddEvent(newEvent);
		}*/
	}

	/// <summary>
	/// Handles the TriggerExitEvent received from the OnTriggerExitComponent to check if a entity marked with 
	/// a ConnectPuzzleCubeComponent has left the modul objects trigger. 
	/// Fires a SphereDeactEvent to inform other systems which sphere is going to be deactivated and to deactivate the control modul.
	/// </summary>
	void Handle(TriggerExitEvent e) {

		if (e.egoComponent1.HasComponents<ModulComponent>() && 
			e.egoComponent2.HasComponents<ConnectPuzzleCubeComponent>()) {

			Debug.Log(e.egoComponent2.name + " left modul");
			e.egoComponent1.GetComponent<ModulComponent>().objectOnModul = null;
			var newEvent = new SphereDeactEvent(e.egoComponent2.GetComponent<Renderer>().material.color);
			EgoEvents<SphereDeactEvent>.AddEvent(newEvent);
		}
		else if (e.egoComponent1.HasComponents<ConnectPuzzleCubeComponent>() &&
			e.egoComponent2.HasComponents<ModulComponent>()) {

			Debug.Log(e.egoComponent2.name + " left modul");
			e.egoComponent2.GetComponent<ModulComponent>().objectOnModul = null;
			var newEvent = new SphereDeactEvent(e.egoComponent1.GetComponent<Renderer>().material.color);
			EgoEvents<SphereDeactEvent>.AddEvent(newEvent);
		}
	}

	/*void Handle (DestroyObjectOnModulEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, BoxCollider, OnTriggerEnterComponent, OnTriggerExitComponent, ModulComponent) => {

			Debug.Log("Destroy");
			GameObject.Destroy(ModulComponent.objectOnModul);

		});
	}*/

	/// <summary>
	/// Instantiates the modul entity.
	/// </summary>
	void InstantiateModul() {
		
		var modul = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Modul")) as GameObject);
		modul.gameObject.name = "Modul";
		Ego.AddComponent<OnTriggerEnterComponent>(modul);
		Ego.AddComponent<OnTriggerExitComponent>(modul);
		Ego.AddComponent<ModulComponent>(modul);

		EgoEvents<TriggerEnterEvent>.AddHandler(Handle);
		EgoEvents<TriggerExitEvent>.AddHandler(Handle);

		modul.GetComponent<BoxCollider>().isTrigger = true;
	}
}