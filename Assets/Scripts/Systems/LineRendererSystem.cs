using UnityEngine;

/// <summary>
/// The LineRendererSystem draws and deletes a line when a sphere from the connect puzzle was activated.
/// </summary>
public class LineRendererSystem : EgoSystem < EgoConstraint<Transform, LineRenderer, DrawLineDataComponent> > {
	public override void Start() {

		EgoEvents<SphereActEvent>.AddHandler(Handle);
		EgoEvents<SphereDeactEvent>.AddHandler(Handle);
		EgoEvents<ConnectPuzzleSolvedEvent>.AddHandler(Handle);
	}

	/// <summary>
	/// Draws or deletes a line from a given startpoint until an defined endpoint is reached.  
	/// </summary>
	public override void Update() {

		constraint.ForEachGameObject((EgoComponent, Transform, LineRenderer, DrawLineDataComponent) => {
			// Checks the conditions to draw a line
			if (DrawLineDataComponent.drawLine == true && 
				DrawLineDataComponent.drawCounter < DrawLineDataComponent.defDist) {

				// Debug.Log("Draw Line");
				// Calculates the points along the line and renders them in the scene. 
				DrawLineDataComponent.drawCounter += 1 / DrawLineDataComponent.defDrawLineSpeed;
				float x = Mathf.Lerp(0, DrawLineDataComponent.defDist, DrawLineDataComponent.drawCounter);
				Vector3 pointAlongeLine = x * Vector3.Normalize(DrawLineDataComponent.defEndPoint - DrawLineDataComponent.defStartPoint) + DrawLineDataComponent.defStartPoint;
				LineRenderer.SetPosition(1, pointAlongeLine);
			}
			// Checks the conditions to delete a line 
			if (DrawLineDataComponent.deleteLine == true && DrawLineDataComponent.solved == false && 
				DrawLineDataComponent.deleteCounter < DrawLineDataComponent.defDist) {

				// Debug.Log("Delete Line");
				// Calculates the points along the line in reversed order to delete the line.
				DrawLineDataComponent.deleteCounter += 1 / DrawLineDataComponent.defDrawLineSpeed;
				float x = Mathf.Lerp(0, DrawLineDataComponent.defDist, DrawLineDataComponent.deleteCounter);
				Vector3 pointAlongeLine = x * Vector3.Normalize(DrawLineDataComponent.defStartPoint - DrawLineDataComponent.defEndPoint) + DrawLineDataComponent.defEndPoint;
				LineRenderer.SetPosition(1, pointAlongeLine);
			}
        });
	}
	/// <summary>
	/// Fires a raycast at each frame and checks if it hits a collider. If this happens, the line will be drawn to the raycasts 
	/// point of impact and the system fires a SphereHitEvent with the hitted object given as parameter. 
	/// </summary>
	public override void FixedUpdate() {

		constraint.ForEachGameObject((EgoComponent, Transform, LineRenderer, DrawLineDataComponent) => {
			// Checks which line is currently active and not solved yet.
			if(DrawLineDataComponent.isActive == true && DrawLineDataComponent.solved == false) {

				RaycastHit hit;
				// Fires raycasts at each frame. 
				if (Physics.Raycast(Transform.position,Transform.right, out hit)) {

					if (hit.collider) {
						// Shortens line to the hit position.
						Debug.Log("Laser hit at " + hit.collider.name);
						LineRenderer.SetPosition(1,new Vector3(hit.distance, 0, 0));
						// Fires new SphereHitEvent 
						var e = new SphereHitEvent(hit.collider.gameObject);
						EgoEvents<SphereHitEvent>.AddEvent(e);
					}
				}
				else { 
					// Draws the line back to its default endpoint. 
					LineRenderer.SetPosition(1, DrawLineDataComponent.defEndPoint);
				}
			}
		});
	}

	/// <summary>
	/// Handles the SphereActEvent received from the ModulSystem to activate the line which is going to be drawn.  
	/// </summary>
	void Handle(SphereActEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, LineRenderer, DrawLineDataComponent) => {
			// Red line is ready to be drawn.
			if (e.cubeColor == Color.red) {

				if (DrawLineDataComponent.color.Equals("red")) {
					//Debug.Log("Red line drawing activated");
					SetActivateData(DrawLineDataComponent);
				}
			}
			// Green line is ready to be drawn.
			else if (e.cubeColor == Color.green) {

				if (DrawLineDataComponent.color.Equals("green")) {
					//Debug.Log("Green line drawing activated");
					SetActivateData(DrawLineDataComponent);
				}
			}
			// Blue line is ready to be drawn.
			else if (e.cubeColor == Color.blue) {

				if (DrawLineDataComponent.color.Equals("blue")) {
					//Debug.Log("Blue line drawing activated");
					SetActivateData(DrawLineDataComponent);
				}
			
			}
			// Yellow line is ready to be drawn. 
			else if (e.cubeColor == Color.yellow) {

				if (DrawLineDataComponent.color.Equals("yellow")) {
					//Debug.Log("Yellow line drawing activated");
					SetActivateData(DrawLineDataComponent);
				}
			}
        });
	}

	/// <summary>
	/// Handles the SphereDeactEvent received from the ModulSystem to deactivate the line which is then going to be deleted.  
	/// </summary>
	void Handle(SphereDeactEvent e) {

		constraint.ForEachGameObject( (EgoComponent, Transform, LineRenderer, DrawLineDataComponent) => {
			// Red line is ready to be deleted.
			if (e.lastCubeColor == Color.red) {

				if (DrawLineDataComponent.color.Equals("red")) {
					//Debug.Log("Red line drawing deactivated");
					SetDeactivateData(DrawLineDataComponent);
				}
			}
			// Green line is ready to be deleted.
			else if (e.lastCubeColor == Color.green) {

				if (DrawLineDataComponent.color.Equals("green")) {
					//Debug.Log("Green line drawing deactivated");
					SetDeactivateData(DrawLineDataComponent);
				}
			}
			// Blue line is ready to be deleted.
			else if (e.lastCubeColor == Color.blue) {

				if (DrawLineDataComponent.color.Equals("blue")) {
					//Debug.Log("Blue line drawing deactivated");
					SetDeactivateData(DrawLineDataComponent);
				}
			}
			// Yellow line is ready to be deleted.
			else if (e.lastCubeColor == Color.yellow) {

				if (DrawLineDataComponent.color.Equals("yellow")) {
					//Debug.Log("Yellow line drawing deactivated");
					SetDeactivateData(DrawLineDataComponent);
				}
			}
        });
	}

	/// <summary>
	/// Handles the ConnectPuzzleSolvedEvent received from the ConnectPuzzleSystem to visualize that the current sphere is 
	/// connected with its solution.
	/// </summary>
	void Handle(ConnectPuzzleSolvedEvent e) {

		constraint.ForEachGameObject( (EgoComponent, Transform, LineRenderer, DrawLineDataComponent) => {

			if (DrawLineDataComponent.isActive) {

				DrawLineDataComponent.solved = true;
			}
        });
	}

	/// <summary>
	/// Helper method that sets all conditions to draw a line 
	/// </summary>
	void SetActivateData(DrawLineDataComponent dld) {
		
		dld.drawLine = true;
		dld.isActive = true;
		dld.deleteLine = false;
		dld.deleteCounter = 0;
	}

	/// <summary>
	/// Helper method that sets all conditions to delete a line 
	/// </summary>
	void SetDeactivateData(DrawLineDataComponent dld) {
		
		dld.isActive = false;
		dld.deleteLine = true;
		dld.drawLine = false;
		dld.drawCounter = 0;
	}
}