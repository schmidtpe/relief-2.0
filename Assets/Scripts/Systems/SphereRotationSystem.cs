using UnityEngine;

/// <summary>
/// The SphereRotationSystem enables to change the direction of a connect puzzle sphere entity by rotating it through the users 
/// interaction with the control modul.
/// </summary>
public class SphereRotationSystem : EgoSystem< EgoConstraint<Transform, SphereCollider, ConnectPuzzleSphereComponent> > {

	public override void Start()
	{
		EgoEvents<SphereActEvent>.AddHandler(Handle);
		EgoEvents<SphereDeactEvent>.AddHandler(Handle);
		EgoEvents<StartRotationEvent>.AddHandler(Handle);
		EgoEvents<StopRotationEvent>.AddHandler(Handle);
	}

	/// <summary>
	/// Rotates the sphere to the left or right on the y-axis at each frame.
	/// </summary>
	public override void Update() {
		
		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ConnectPuzzleSphereComponent) => {

			if(ConnectPuzzleSphereComponent.isSolved == false) {

				if(ConnectPuzzleSphereComponent.rotateRight == true) {

					Transform.Rotate(new Vector3(0f,0.1f,0f));
				}
				else if(ConnectPuzzleSphereComponent.rotateLeft == true) {

					Transform.Rotate(new Vector3(0f,-0.1f,0f));
				}
			}
        });
	}

	/// <summary>
	/// Handles a SphereActEvent received from the ModulSystem to activate a sphere by the events given color parameter. 
	/// </summary>
	void Handle(SphereActEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ConnectPuzzleSphereComponent) => {

			if (EgoComponent.GetComponent<Renderer>().material.color == e.cubeColor) {

				ConnectPuzzleSphereComponent.isActive = true;
			}
        });

	}

	/// <summary>
	/// Handles a SphereActEvent received from the ModulSystem to deactivate any active sphere.
	/// </summary>
	void Handle(SphereDeactEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ConnectPuzzleSphereComponent) => {

				ConnectPuzzleSphereComponent.isActive = false;
        });
	}

	/// <summary>
	/// Handles a StartRotationEvent received from the ControlModulSystem to enable the rotation of the active sphere. 
	/// </summary>
	void Handle (StartRotationEvent e) {
		
		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ConnectPuzzleSphereComponent) => {

			if (ConnectPuzzleSphereComponent.isActive == true) {
				// Checks if the sphere should rotating to the right.
				if(e.controlModul.name.Equals("Control Modul Right")) {

					ConnectPuzzleSphereComponent.rotateRight = true;
				}
				// Checks if the sphere should rotating to the left.
				else if(e.controlModul.name.Equals("Control Modul Left")) {

					ConnectPuzzleSphereComponent.rotateLeft = true;
				}
			}
        });
	}

	/// <summary>
	/// Handles a StopRotationEvent received from the ControlModulSystem to disable the rotation of the active sphere. 
	/// </summary>
	void Handle(StopRotationEvent e) {
		
		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ConnectPuzzleSphereComponent) => {

			// Checks if the sphere should not rotating to the right
			if (e.controlModul.name.Equals("Control Modul Right")) {

				ConnectPuzzleSphereComponent.rotateRight = false;
			}
			// Checks if the sphere should not rotating to the left 
			else if(e.controlModul.name.Equals("Control Modul Left")) {

				ConnectPuzzleSphereComponent.rotateLeft = false;
			}
        });

	}
}