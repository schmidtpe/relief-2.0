using UnityEngine;

/// <summary>
/// The PatternPuzzleCollisionSystem detects collisions between the pattern puzzle objects and the controller entities to
/// enable the users interaction with the second puzzle. 
/// </summary>
public class PatternPuzzleCollisionSystem : EgoSystem < 
	EgoConstraint<Transform, PatternPuzzleObjComponent, OnCollisionEnterComponent, OnCollisionExitComponent, Rigidbody>
>{
	public override void Start() {

		EgoEvents<CollisionEnterEvent>.AddHandler(Handle);
		
	}

	/// <summary>
	/// Handles the CollisionEnterEvent received from the OnCollisionEnterComponent to detect the collision of a controller and 
	/// an object marked with a PatternPuzzleObjComponent.
	/// Fires a PatternPuzzleObjHitEvent, if this happens. 
	/// </summary>
	void Handle(CollisionEnterEvent e) {

		if (e.egoComponent1.HasComponents<PatternPuzzleObjComponent>() && 
			e.egoComponent2.HasComponents<ControllerComponent>()) {

			var hitEvent = new PatternPuzzleObjHitEvent(e.egoComponent1);
			EgoEvents<PatternPuzzleObjHitEvent>.AddEvent(hitEvent);
			// Debug.Log("Controller hitted pattern puzzle obj");

		}
		else if (e.egoComponent1.HasComponents<ControllerComponent>() && 
			e.egoComponent2.HasComponents<PatternPuzzleObjComponent>()) {

			var hitEvent = new PatternPuzzleObjHitEvent(e.egoComponent2);
			EgoEvents<PatternPuzzleObjHitEvent>.AddEvent(hitEvent);
			// Debug.Log("Controller hitted puzzle obj");
		}

	}
}