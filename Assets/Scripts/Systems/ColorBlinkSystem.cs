using UnityEngine;

/// <summary>
/// The ColorBlinkSystem switches the color of the objects from the pattern puzzle.
/// </summary>
public class ColorBlinkSystem : EgoSystem<EgoConstraint<Transform, PatternPuzzleObjComponent, OnCollisionEnterComponent, 
	OnCollisionExitComponent>>
{
	public override void Start() {

		EgoEvents<ColorBlinkEvent>.AddHandler(Handle);
		EgoEvents<TurnColorOffEvent>.AddHandler(Handle);
	}

	/// <summary>
	/// Handles the ColorBlinkEvent received from the PatternPuzzleSystem to switch on the color of an pattern puzzle object. 
	/// </summary>
	void Handle(ColorBlinkEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleObjComponent, OnCollisionEnterComponent, 
			OnCollisionExitComponent) => {

			switch(e.color) {
				// Turns on the color of the object marked as red.
				case 0:
					if (PatternPuzzleObjComponent.color.Equals("red")) {
						EgoComponent.gameObject.GetComponent<Renderer>().material.color = Color.red;
					}
				break;
				// Turns on the color of the object marked as green. 
				case 1:
					if (PatternPuzzleObjComponent.color.Equals("green")) {
						EgoComponent.gameObject.GetComponent<Renderer>().material.color = Color.green;
					}
				break;
				// Turns on the color of the object marked as blue.
				case 2:
					if (PatternPuzzleObjComponent.color.Equals("blue")) {
						EgoComponent.gameObject.GetComponent<Renderer>().material.color = Color.blue;
					}
				break;
				// Turns on the color of the object marked as yellow. 
				case 3:
					if (PatternPuzzleObjComponent.color.Equals("yellow")) {
						EgoComponent.gameObject.GetComponent<Renderer>().material.color = Color.yellow;
					}
				break;
			}
		});
	}

	/// <summary>
	/// Handles the TurnColorOffEvent received from the PatternPuzzleSystem to switch off the color of an pattern puzzle object. 
	/// </summary>
	void Handle (TurnColorOffEvent e) {

			constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleObjComponent, OnCollisionEnterComponent, 
				OnCollisionExitComponent) => {
				// Sets the color back to white 
				EgoComponent.gameObject.GetComponent<Renderer>().material.color = Color.white;
			});
	}
}