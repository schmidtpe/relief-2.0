﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The ConnectPuzzleSystem initialises all puzzle entities and checks if each sphere is connected with its solution.
/// </summary>
public class ConnectPuzzleSystem : EgoSystem< EgoConstraint<Transform, SphereCollider, ConnectPuzzleSphereComponent> > {

	public override void Start () {

		EgoEvents<SphereHitEvent>.AddHandler(Handle);

		InitialisePuzzleCubes();
		InitialisePuzzleSpheresAndLines();
	}

	/// <summary>
	/// Handles the SphereHitEvent received from the LineRendererSystem and checks if the conditions are met 
	/// for connecting two spheres together.
	/// Fires a ConnectPuzzleSolvedEvent with a integer-value as parameter to inform other systems which 
	/// solution was achieved. 
	/// </summary>
	void Handle(SphereHitEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ConnectPuzzleSphereComponent) => {

			if (ConnectPuzzleSphereComponent.isActive == true && 
				ConnectPuzzleSphereComponent.isSolved == false) {
				// First solution achieved (red & green).
				if (ConnectPuzzleSphereComponent.name.Equals("Red Sphere") && 
					e.hittedSphere.name.Equals("Green Sphere")) {
					
					ConnectPuzzleSphereComponent.isSolved = true;
					// Debug.Log("Red solution");
					// var destroyEvent = new DestroyObjectOnModul();
					// EgoEvents<DestroyObjectOnModul>.AddEvent(destroyEvent);
					var ev = new ConnectPuzzleSolvedEvent(0);
					EgoEvents<ConnectPuzzleSolvedEvent>.AddEvent(ev);
				}
				// Second solution achieved (green & yellow).
				else if (ConnectPuzzleSphereComponent.name.Equals("Green Sphere") && 
					e.hittedSphere.name.Equals("Yellow Sphere")) {
					
					ConnectPuzzleSphereComponent.isSolved = true;
					// Debug.Log("Green solution");
					// var destroyEvent = new DestroyObjectOnModul();
					// EgoEvents<DestroyObjectOnModul>.AddEvent(destroyEvent);
					var ev = new ConnectPuzzleSolvedEvent(1);
					EgoEvents<ConnectPuzzleSolvedEvent>.AddEvent(ev);
				}
				// Third solution achieved (blue and red).
				else if (ConnectPuzzleSphereComponent.name.Equals("Blue Sphere") && 
					e.hittedSphere.name.Equals("Red Sphere")) {

					ConnectPuzzleSphereComponent.isSolved = true;
					// Debug.Log("Blue solution");
					// var destroyEvent = new DestroyObjectOnModul();
					// EgoEvents<DestroyObjectOnModul>.AddEvent(destroyEvent);
					var ev = new ConnectPuzzleSolvedEvent(2);
					EgoEvents<ConnectPuzzleSolvedEvent>.AddEvent(ev);
				}
				// Fourth solution achieved (yellow & blue).
				else if (ConnectPuzzleSphereComponent.name.Equals("Yellow Sphere") && 
					e.hittedSphere.name.Equals("Blue Sphere")) {

					ConnectPuzzleSphereComponent.isSolved = true;
					// Debug.Log("Yellow solution");
					// var destroyEvent = new DestroyObjectOnModul();
					// EgoEvents<DestroyObjectOnModul>.AddEvent(destroyEvent);
					var ev = new ConnectPuzzleSolvedEvent(3);
					EgoEvents<ConnectPuzzleSolvedEvent>.AddEvent(ev);
				}
				else {
					// Debug.Log("No solution");
				}
			}
		});
	}

	/// <summary>
	/// Initialises the puzzle cubes on the grescaling wall for simpler prototyping.
	/// </summary>
	void InitialisePuzzleCubes() {

		var list = new List<Color>{Color.red, Color.green, Color.blue, Color.yellow};
		float s = -1.3f;
		for (int i = 0; i < list.Count; i++) {

			s = s + 0.5f;
			var cube = Ego.AddGameObject(GameObject.CreatePrimitive(PrimitiveType.Cube));
			cube.name = "Puzzle Cube " + i;
			cube.GetComponent<Renderer>().material.color = list[i];

			cube.transform.position = new Vector3(s, 0.5f, 0.2f);
			cube.transform.rotation = Quaternion.identity;
			cube.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

			Ego.AddComponent<Rigidbody>(cube);
			Ego.AddComponent<ConnectPuzzleCubeComponent>(cube);
			Ego.AddComponent<CatchableComponent>(cube);
		}
	}

	/// <summary>
	/// Initialises the puzzle spheres and lines from prefab.
	/// </summary>
	void InitialisePuzzleSpheresAndLines() {
		// Initialises sphere objects. 
		var redSphere = Ego.AddGameObject(GameObject.CreatePrimitive(PrimitiveType.Sphere));
		var greenSphere = Ego.AddGameObject(GameObject.CreatePrimitive(PrimitiveType.Sphere));
		var blueSphere = Ego.AddGameObject(GameObject.CreatePrimitive(PrimitiveType.Sphere));
		var yellowSphere = Ego.AddGameObject(GameObject.CreatePrimitive(PrimitiveType.Sphere));
		// Resets the name of each sphere.
		redSphere.name = "Red Sphere";
		greenSphere.name = "Green Sphere";
		blueSphere.name = "Blue Sphere";
		yellowSphere.name = "Yellow Sphere";
		// Adds a PuzzleSphereComponent to each sphere. 
		Ego.AddComponent<ConnectPuzzleSphereComponent>(redSphere);
		Ego.AddComponent<ConnectPuzzleSphereComponent>(greenSphere);
		Ego.AddComponent<ConnectPuzzleSphereComponent>(blueSphere);
		Ego.AddComponent<ConnectPuzzleSphereComponent>(yellowSphere);
		// Sets the color of each sphere.
		redSphere.GetComponent<Renderer>().material.color = Color.red;
		greenSphere.GetComponent<Renderer>().material.color = Color.green;
		blueSphere.GetComponent<Renderer>().material.color = Color.blue;
		yellowSphere.GetComponent<Renderer>().material.color = Color.yellow;
		// Sets the position of each sphere above the obelisk objects.
		redSphere.transform.position = new Vector3 (4f, 6.8f, 4f);
		greenSphere.transform.position = new Vector3 (4f, 6.8f, -4f);
		blueSphere.transform.position = new Vector3 (-4f, 6.8f, -4f);
		yellowSphere.transform.position = new Vector3 (-4f, 6.8f, 4f);
		// Sets the rotation of each sphere, so the lines will show in different directions when activated.
		redSphere.transform.localRotation = Quaternion.Euler(0, 50, 0);
		greenSphere.transform.localRotation = Quaternion.Euler(0, -200, 0);
		blueSphere.transform.localRotation = Quaternion.Euler(0, 250, 0);
		yellowSphere.transform.localRotation = Quaternion.Euler(0, -45, 0);
		// Initialises line objects from prefab. 
		var redLine = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Red Line")) as GameObject);
		var greenLine = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Green Line")) as GameObject);
		var blueLine = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Blue Line")) as GameObject);
		var yellowLine = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Yellow Line")) as GameObject);
		// Resets the name of each line object.  
		redLine.name = "Red Line";
		greenLine.name = "Green Line";
		blueLine.name = "Blue Line";
		yellowLine.name = "Yellow Line";
		// Adds DrawLineDataComponent to each line. 
		Ego.AddComponent<DrawLineDataComponent>(redLine);
		Ego.AddComponent<DrawLineDataComponent>(greenLine);
		Ego.AddComponent<DrawLineDataComponent>(blueLine);
		Ego.AddComponent<DrawLineDataComponent>(yellowLine);
		// Sets the color string value for each line object. 
		redLine.GetComponent<DrawLineDataComponent>().color = "red";
		greenLine.GetComponent<DrawLineDataComponent>().color = "green";
		blueLine.GetComponent<DrawLineDataComponent>().color = "blue";
		yellowLine.GetComponent<DrawLineDataComponent>().color = "yellow";
		// Parenting lines to spheres. 
		redLine.transform.parent = redSphere.transform;
		redLine.transform.position = redSphere.transform.position;
		redLine.transform.rotation = redSphere.transform.rotation;
		// Sets each child position and rotation to the parents one. 
		greenLine.transform.parent 	= greenSphere.transform;
		greenLine.transform.position = greenSphere.transform.position;
		greenLine.transform.rotation = greenSphere.transform.rotation;
		blueLine.transform.parent = blueSphere.transform;
		blueLine.transform.position = blueSphere.transform.position;
		blueLine.transform.rotation = blueSphere.transform.rotation;
		yellowLine.transform.parent = yellowSphere.transform;
		yellowLine.transform.position = yellowSphere.transform.position;
		yellowLine.transform.rotation = yellowSphere.transform.rotation;
	}
}
