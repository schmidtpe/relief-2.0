﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The RotationSystems initialises objects which a rotating around the user. He can grab them and put in in the modul to activate 
/// the spheres of the connect puzzle. 
/// </summary>
public class RotationSystem : EgoSystem < 
	EgoConstraint<Transform, RotationAbleComponent, CatchableComponent, ConnectPuzzleCubeComponent, Rigidbody> 
>{

	public override void Start () {

		EgoEvents<CatchEvent>.AddHandler(Handle);

		var colors = new List<Color>{Color.red, Color.green, Color.blue, Color.yellow};

		InstantiatePuzzleCubes(colors);

	}

	/// <summary>
	/// Updates the object rotation around a given point and the rotation around itself at each frame. 
	/// </summary>
	public override void Update () {

		 constraint.ForEachGameObject((EgoComponent, Transform, RotationAbleComponent, CatchableComponent, ConnectPuzzleCubeComponent, Rigidbody) => {

				Transform.Rotate(RotationAbleComponent.selfRotationSpeed);
				Transform.RotateAround(Vector3.up, RotationAbleComponent.spinAxis, RotationAbleComponent.rotateAroundSpeed);		
        });
	}

	/// <summary>
	/// Handles the CatchEvent received from the CatchSystem to remove the grabbed object from the system to stop rotating it.
	void Handle(CatchEvent e) {

		 constraint.ForEachGameObject((EgoComponent, Transform, RotationAbleComponent, CatchableComponent, ConnectPuzzleCubeComponent, Rigidbody) => {

			 if(CatchableComponent.isCatched == true) {
				 	Rigidbody.useGravity = true;
					Ego.DestroyComponent<RotationAbleComponent>(EgoComponent);
			 }	
        });
	}

	/// <summary>
	/// Instantiates the rotating connect puzzle cubes entities. 
	/// </summary>
	void InstantiatePuzzleCubes(List<Color> list) {

		int totalCubes = 8;
		float totalDistance = 2f;

		System.Random rdm = new System.Random();

		for (int i = 0; i < totalCubes; i++) {


			var perc = i / (float) totalCubes;
			var sin = Mathf.Sin(perc * Mathf.PI/2);

			var x = .1f + sin * totalDistance;
			var y = 2f;
			var z = 0;

			var cubeObj = Ego.AddGameObject(GameObject.CreatePrimitive(PrimitiveType.Cube));
			var rotationAbleComp = Ego.AddComponent<RotationAbleComponent>(cubeObj);

			var rigidBodyComp = Ego.AddComponent<Rigidbody>(cubeObj);
			var rendererComp = cubeObj.GetComponent<Renderer>();

			var rdmNr = rdm.Next(0, list.Count);

			Ego.AddComponent<CatchableComponent>(cubeObj);
			Ego.AddComponent<ConnectPuzzleCubeComponent>(cubeObj);
				
			cubeObj.name = "Rotateable Cube " + i;
			cubeObj.transform.position = new Vector3(x,y,z);
			cubeObj.transform.rotation = Quaternion.identity;
			cubeObj.transform.localScale = new Vector3(.1f, .1f, .1f);

			rotationAbleComp.selfRotationSpeed = new Vector3(Random.value, Random.value, Random.value);
			rotationAbleComp.spinAxis = Vector3.up;
			rotationAbleComp.spinAxis.x = (Random.value - Random.value);
			rotationAbleComp.rotateAroundSpeed = 0.1f;

			rigidBodyComp.useGravity = false;

			rendererComp.material.color = list[rdmNr];
		}
	}
}
