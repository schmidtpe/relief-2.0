using UnityEngine;

/// <summary>
/// The ConnectPuzzleSolutionSystem checks if all conditions are met to complete the first puzzle. 
/// </summary>
public class ConnectPuzzleSolutionSystem : EgoSystem< EgoConstraint<Transform, ConnectPuzzleSolutionComponent> >
{
	public override void Start() {

		var connectPuzzleSolutionObj = Ego.AddGameObject(new GameObject());
		connectPuzzleSolutionObj.name = "Connect Puzzle Solution Object";
		Ego.AddComponent<ConnectPuzzleSolutionComponent>(connectPuzzleSolutionObj);

		EgoEvents<ConnectPuzzleSolvedEvent>.AddHandler(Handler);
		
	}

	/// <summary>
	/// Checks at every frame, if all conditions are met to complete the first puzzle, therefore the helper method 
	/// IsConnectPuzzleSolved is used.
	/// Fires an InitPatternPuzzleEvent and stops checking when all conditions are met. 
	/// </summary>
	public override void Update() {

			constraint.ForEachGameObject((EgoComponent, Transform, ConnectPuzzleSolutionComponent) => {

			if (IsConnectPuzzleSolved(ConnectPuzzleSolutionComponent.solutions) == true && 
					ConnectPuzzleSolutionComponent.solutionChecked == false) {

					// Debug.Log("Connect puzzle completely solved");
					ConnectPuzzleSolutionComponent.solutionChecked = true;
					var e = new InitPatternPuzzleEvent();
					EgoEvents<InitPatternPuzzleEvent>.AddEvent(e);
				}
			});
	}

	/// <summary>
	/// Handles the ConnectPuzzleSolvedEvent received from the ConnectPuzzleSystem to set the value of the solutions-array at 
	/// the passed index to true.
	/// </summary>
	void Handler (ConnectPuzzleSolvedEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, ConnectPuzzleSolutionComponent) => {

			switch(e.index) {
				// First condition met.
				case 0:
				ConnectPuzzleSolutionComponent.solutions[0] = true;
				break;
				// Second condition met.
				case 1:
				ConnectPuzzleSolutionComponent.solutions[1] = true;
				break;
				// Third condition met.
				case 2:
				ConnectPuzzleSolutionComponent.solutions[2] = true;
				break;
				// Fourth condition met.
				case 3:
				ConnectPuzzleSolutionComponent.solutions[3] = true;
				break;
			}

		});
	}

	/// <summary>
	/// Determines whether the specified array's values are all set to true. 
	/// </summary>
	bool IsConnectPuzzleSolved(bool[] array) {

		for (int i = 0; i < array.Length; i++) {
			
			if (array[i] == false) {
				 return false;
			 }
		}
		return true;
	}
}