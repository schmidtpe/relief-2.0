using UnityEngine;

/// <summary>
/// The ControlModulSystem initialises the entities to control the spheres of the connect puzzle and
/// communicates with the SphereRotationSystem, so the spheres can be connect together. 
/// </summary>
public class ControlModulSystem : EgoSystem< EgoConstraint<Transform, SphereCollider, ParticleSystem, ControlModulComponent, 
	OnTriggerStayComponent, OnTriggerExitComponent> 
>{
	
	public override void Start() {
		
		EgoEvents<SphereActEvent>.AddHandler(Handle);
		EgoEvents<SphereDeactEvent>.AddHandler(Handle);
		EgoEvents<TriggerStayEvent>.AddHandler(Handle);
		EgoEvents<TriggerExitEvent>.AddHandler(Handle);

		InitialiseControlModul();
	}

	/// <summary>
	/// Handles the SphereActEvent received from the ModulSystem to activate the control modul objects. 
	/// </summary>
	void Handle(SphereActEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ParticleSystem, ControlModul, 
			OnTriggerStayComponent, OnTriggerExitComponent) => {
	
				EgoComponent.gameObject.SetActive(true);
        } );
	}

	/// <summary>
	/// Handles the SphereDeactEvent received from the ModulSystem to deactivate the control modul objects. 
	/// </summary>
	void Handle(SphereDeactEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, SphereCollider, ParticleSystem, ControlModul, 
			OnTriggerStayComponent, OnTriggerExitComponent) => {

				EgoComponent.gameObject.SetActive(false);

        } );

	}
	/// <summary>
	/// Handles the TriggerStayEvent received from the OnTriggerStayComponent to check if a controller stays in the
	/// control modul object's collider and fires a StartRotationEvent if this happens.
	/// </summary>
	void Handle(TriggerStayEvent e) {
		
		if (e.egoComponent1.HasComponents<ControllerComponent>() && e.egoComponent2.HasComponents<ControlModulComponent>()) {
			
			var newEvent = new StartRotationEvent(e.egoComponent2.gameObject);
			EgoEvents<StartRotationEvent>.AddEvent(newEvent);
		}

		else if(e.egoComponent2.HasComponents<ControllerComponent>() && e.egoComponent1.HasComponents<ControlModulComponent>()) {
			
			var newEvent = new StartRotationEvent(e.egoComponent1.gameObject);
			EgoEvents<StartRotationEvent>.AddEvent(newEvent);
		}
	}

	/// <summary>
	/// Handles the TriggerExitEvent received from the OnTriggerExitComponent to check if a controller has left the
	/// control modul object's collider and fires a StopRotationEvent if this happens.
	/// </summary>
	void Handle(TriggerExitEvent e) {
		
		if (e.egoComponent1.HasComponents<ControllerComponent>() && e.egoComponent2.HasComponents<ControlModulComponent>()) {

			var newEvent = new StopRotationEvent(e.egoComponent2.gameObject);
			EgoEvents<StopRotationEvent>.AddEvent(newEvent);
		}
		else if(e.egoComponent2.HasComponents<ControllerComponent>() && e.egoComponent1.HasComponents<ControlModulComponent>()) {

			var newEvent = new StopRotationEvent(e.egoComponent1.gameObject);
			EgoEvents<StopRotationEvent>.AddEvent(newEvent);
		}
	}

	/// <summary>
	/// Initialises the control modul entities.
	/// </summary>
	void InitialiseControlModul() {
		// Initialises left control modul from prefab. 
		var leftControlModul = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Control Modul Left") as GameObject));
		leftControlModul.name = "Control Modul Left";
		Ego.AddComponent<ControlModulComponent>(leftControlModul);
		Ego.AddComponent<OnTriggerStayComponent>(leftControlModul);
		Ego.AddComponent<OnTriggerExitComponent>(leftControlModul);
		// Initialises right control modul from prefab.
		var rightControlModul = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Control Modul Right") as GameObject));
		rightControlModul.gameObject.name = "Control Modul Right";
		Ego.AddComponent<ControlModulComponent>(rightControlModul);
		Ego.AddComponent<OnTriggerStayComponent>(rightControlModul);
		Ego.AddComponent<OnTriggerExitComponent>(rightControlModul);
		// Waits for the SphereActEvent to get activated. 
		leftControlModul.gameObject.SetActive(false);
		rightControlModul.gameObject.SetActive(false);
	}
}