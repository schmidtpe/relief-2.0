using UnityEngine;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// The PatternPuzzleSystem enables the logic of the second puzzle. Therefore it generates a random pattern of colors that are 
/// build on one another. For Example: If the first round sets the color to blue then the second round could be the pattern of 
/// blue, yellow and the third round could be the pattern of blue, yellow, green. The user then have to replay every pattern-round
/// to solve the puzzle. 
/// </summary>
public class PatternPuzzleSystem : EgoSystem < 
	EgoConstraint<Transform, PatternPuzzleComponent>
>{
	
	public override void Start() {

		EgoEvents<PatternPuzzleObjHitEvent>.AddHandler(Handle);
		EgoEvents<InitPatternPuzzleEvent>.AddHandler(Handle);
	}

	public override void Update() {

		// Note: This is a workaround to start the thread when the InitPatternPuzzleEvent was fired because 
		// constraint.ForEachGameObject() won't get invoked in the Start()-function like it should.
		constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleComponent) => {

			if(PatternPuzzleComponent.threadStarted == false) {
				
					PatternPuzzleComponent.patternThread = new Thread(PatternLoop);
					PatternPuzzleComponent.patternThread.Start();
					PatternPuzzleComponent.threadStarted = true;
			}
        });

		CheckCollisions();
	}

	/// <summary>
	/// Starts the thread which inovkes the current pattern round. The System fires a ColorBlinkEvent, if the pattern color 
	/// should be switched on. The TurnColorOffEvent informs other systems to switch off the color after some sleep time. 
	/// </summary>
	void PatternLoop() {
		
		constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleComponent) => {

			// Debug.Log("New PatternLoop started");

			// If the pattern is playing, the user can't interact with the pattern objects.
			PatternPuzzleComponent.patternIsPlaying = true;

			Thread.Sleep(2000);

			foreach (int colorIndex in PatternPuzzleComponent.pattern) {

				switch(colorIndex) {
					// Current pattern color is red.
					case 0:
						var colorOn1 = new ColorBlinkEvent(0);
						EgoEvents<ColorBlinkEvent>.AddEvent(colorOn1);
						// Debug.Log("red color on");
						Thread.Sleep(600);
						var colorOff1 = new TurnColorOffEvent();
						EgoEvents<TurnColorOffEvent>.AddEvent(colorOff1);
						// Debug.Log("red color off");
						break;
					// Current pattern color is green.
					case 1:
						var colorOn2 = new ColorBlinkEvent(1);
						EgoEvents<ColorBlinkEvent>.AddEvent(colorOn2);
						// Debug.Log("green color on");
						Thread.Sleep(600);
						var colorOff2 = new TurnColorOffEvent();
						EgoEvents<TurnColorOffEvent>.AddEvent(colorOff2);
						// Debug.Log("green color off");
						break;
					// Current pattern color is blue.
					case 2:
						var colorOn3 = new ColorBlinkEvent(2);
						EgoEvents<ColorBlinkEvent>.AddEvent(colorOn3);
						// Debug.Log("blue color on");
						Thread.Sleep(600);
						var colorOff3 = new TurnColorOffEvent();
						EgoEvents<TurnColorOffEvent>.AddEvent(colorOff3);
						// Debug.Log("blue color off");
						break;
					// Current pattern color is yellow.
					case 3:
						var colorOn4 = new ColorBlinkEvent(3);
						EgoEvents<ColorBlinkEvent>.AddEvent(colorOn4);
						// Debug.Log("yellow color on");
						Thread.Sleep(600);
						var colorOff4 = new TurnColorOffEvent();
						EgoEvents<TurnColorOffEvent>.AddEvent(colorOff4);
						// Debug.Log("yellow color off");
					break;
				}
				Thread.Sleep(600);
			}
			PatternPuzzleComponent.patternIsPlaying = false;
        });
	}

	/// <summary>
	/// Handles the PatternPuzzleObjHitEvent received from the PatternPuzzleCollisionSystem to store the object which the user 
	/// has been interacting with.
	/// </summary>
	void Handle(PatternPuzzleObjHitEvent e) {

		constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleComponent) => {

			PatternPuzzleComponent.hittedEgoComponent = e.egoComponent;
        });
	}

	/// <summary>
	/// Checks at each frame if the stored object from the PatternPuzzleObjHitEvent is as solution of the current pattern round.
	/// </summary>
	void CheckCollisions() {

		constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleComponent) => {

			if (PatternPuzzleComponent.hittedEgoComponent != null) {

				switch(PatternPuzzleComponent.hittedEgoComponent.GetComponent<PatternPuzzleObjComponent>().color) {
					// The hitted object is red. Was the last color in the pattern red, too?
					case "red":
					TestPattern(0);
					PatternPuzzleComponent.hittedEgoComponent = null;
					break;
					// The hitted object is green. Was the last color in the pattern green, too?
					case "green":
					TestPattern(1);
					PatternPuzzleComponent.hittedEgoComponent = null;
					break;
					// The hitted object is blue. Was the last color in the pattern blue, too?
					case "blue":
					TestPattern(2);
					PatternPuzzleComponent.hittedEgoComponent = null;
					break;
					case "yellow":
					// The hitted object is yellow. Was the last color in the pattern yellow, too?
					TestPattern(3);
					PatternPuzzleComponent.hittedEgoComponent = null;
					break;
				}
			}
        });
	}
	/// <summary>
	/// Tests if the current pattern was solved. If this happens the next color is randomly added to the pattern and the thread is
	/// starting again. If the wrong pattern object was hitted by the user, then the pattern puzzle is starting from the beginning.
	/// Note: No score is implemented yet, so the puzzle has no ending.
	void TestPattern(int color) {

		constraint.ForEachGameObject((EgoComponent, Transform, PatternPuzzleComponent) => {
			// Ignores users interaction when the pattern is playing. 
			if (PatternPuzzleComponent.patternIsPlaying) {
				 return;
		 	}
			// Checks if each pattern object was replayed correctly.
		 	if (PatternPuzzleComponent.pattern[PatternPuzzleComponent.onList] == color) {
			 	PatternPuzzleComponent.onList++;	 
			}
			// Starts the pattern from the beginning when the current pattern was not solved. 
		 	else {
				// Debug.Log("Game over");
				PatternPuzzleComponent.onList = 0;
				PatternPuzzleComponent.pattern = new List<int>();
				PatternPuzzleComponent.threadStarted = false;
			}
			// Adds a new color to the pattern and stars the next pattern round if every pattern object was replayed correctly.
			if (PatternPuzzleComponent.onList >= PatternPuzzleComponent.pattern.Count) {
				// Debug.Log("Next pattern round");
				System.Random rand = new System.Random();
				PatternPuzzleComponent.pattern.Add(rand.Next(0,4));
				PatternPuzzleComponent.onList = 0;
				PatternPuzzleComponent.threadStarted = false;
			}
        });
	 }

	/// <summary>
	/// Handles the InitPatternPuzzleEvent received from the ConnectPuzzleSolutionSystem to initialise the second puzzle when 
	/// the first one was solved. 
	/// </summary>
	void Handle (InitPatternPuzzleEvent e) {

		var cMleft = GameObject.Find("Control Modul Left").GetComponent<EgoComponent>();
		var cMright = GameObject.Find("Control Modul Right").GetComponent<EgoComponent>();
		Ego.DestroyGameObject(cMleft);
		Ego.DestroyGameObject(cMright);

		System.Random rand = new System.Random();

		var logicObj = Ego.AddGameObject(new GameObject());
		var patternComp = Ego.AddComponent<PatternPuzzleComponent>(logicObj);

		logicObj.name = "Pattern Logic";
		patternComp.pattern.Add(rand.Next(0,4));

		var patternObjs = Ego.AddGameObject(GameObject.Instantiate(Resources.Load("Pattern Objects") as GameObject));
		patternObjs.name = "Pattern Objects";
	}
}