﻿using UnityEngine;

/// <summary>
/// The ControllerColliderSystem checks if entities marked with a CachtableComponent collide with controller entities.
/// </summary>
public class ControllerColliderSystem : EgoSystem< 
	EgoConstraint<Transform, SteamVR_TrackedObject, ControllerComponent, ColliderComponent, SphereCollider, OnTriggerEnterComponent, 
		OnTriggerExitComponent, Rigidbody> 
>{

	public override void Start () {

		EgoEvents<CollisionEnterEvent>.AddHandler(Handle);
		EgoEvents<CollisionExitEvent>.AddHandler(Handle);
	}
	/// <summary>
	/// Handles the CollisionEnterEvent received from the OnTriggerEnterComponent.
	/// </summary>
	/// 
	void Handle (CollisionEnterEvent e) {
		// Stores the collided object in the ColliderComponent. 
		if (e.egoComponent1.HasComponents<CatchableComponent>() && e.egoComponent2.HasComponents<ControllerComponent>()) {

			if (e.egoComponent2.GetComponent<ColliderComponent>().collidedObject == null) {

				e.egoComponent2.GetComponent<ColliderComponent>().collidedObject = e.egoComponent1.gameObject;
			}
			
		}
		else if (e.egoComponent1.HasComponents<ControllerComponent>() && e.egoComponent2.HasComponents<CatchableComponent>()) {

			if(e.egoComponent1.GetComponent<ColliderComponent>().collidedObject == null) {
				
				e.egoComponent1.GetComponent<ColliderComponent>().collidedObject = e.egoComponent2.gameObject;
			}
		}
	}

	/// <summary>
	/// Handles the CollisionExitEvent received from the OnTriggerExitComponent.
	/// </summary>
	void Handle (CollisionExitEvent e) {
		// Removes the object stored in the ColliderComponent. 
		if (e.egoComponent1.HasComponents<CatchableComponent>() && e.egoComponent2.HasComponents<ControllerComponent>()) {

			e.egoComponent2.GetComponent<ColliderComponent>().collidedObject = null;
		}
		else if (e.egoComponent1.HasComponents<ControllerComponent>() && e.egoComponent2.HasComponents<CatchableComponent>()) {

			e.egoComponent1.GetComponent<ColliderComponent>().collidedObject = null;
		}
	}
}
