﻿using UnityEngine;

/// <summary>
/// The CatchSystem enables the interaction between controllers and entities marked with a CatchableComponent.
/// </summary>
public class CatchSystem : EgoSystem < EgoConstraint<Transform, SteamVR_TrackedObject, ControllerComponent, 
	ColliderComponent, SphereCollider, OnCollisionEnterComponent, OnCollisionExitComponent, Rigidbody> > {

	public override void Start () {

		EgoEvents<InputEvent>.AddHandler(Handle);

	}

	/// <summary>
	/// Handles the InputEvent received from the InputSystem to grab or release interactive objects and fires a CatchEvent 
	/// when a object is catched.
	/// </summary>
	void Handle (InputEvent e) {
		// Grabs an object when the parameter of the InputEvent is "TriggerOnHold".
		if (e.inputButton.Equals("TriggerOnHold")) {

			constraint.ForEachGameObject((EgoComponent, Transform, SteamVR_TrackedObject, ControllerComponent, 
				ColliderComponent, SphereCollider, OnCollisionEnterComponent, OnCollisionExitComponent, Rigidbody) => {

				if (ColliderComponent.collidedObject != null && ControllerComponent.attachedObject == null) {
	
					ControllerComponent.attachedObject = ColliderComponent.collidedObject;
					ControllerComponent.attachedObject.GetComponent<CatchableComponent>().isCatched = true;
					ControllerComponent.attachedObject.AddComponent<FixedJoint>();
					ControllerComponent.attachedObject.GetComponent<FixedJoint>().connectedBody = ControllerComponent.attachPoint;
					
					var catchEvent = new CatchEvent();
					EgoEvents<CatchEvent>.AddEvent(catchEvent);
					// Debug.Log("Object " + ControllerComponent.attachedObject.name + "grabbed");
				}
			});
		}
		// Releases an object when the parameter of the InputEvent is "TriggerUp".
		if (e.inputButton.Equals("TriggerUp")) {

			constraint.ForEachGameObject((EgoComponent, Transform, SteamVR_TrackedObject, ControllerComponent, ColliderComponent, 
				SphereCollider, OnCollisionEnterComponent, OnCollisionExitComponent, Rigidbody) => {

				if (ControllerComponent.attachedObject != null) {

					Object.DestroyImmediate(ControllerComponent.attachedObject.GetComponent<FixedJoint>());

					ControllerComponent.attachedObject.GetComponent<CatchableComponent>().isCatched = false;

					var rigidbody = ControllerComponent.attachedObject.GetComponent<Rigidbody>();
					var device = ControllerComponent.device;
					var controllerOrigin = SteamVR_TrackedObject.origin ? SteamVR_TrackedObject.origin : 
						SteamVR_TrackedObject.transform.parent;
					
					// Sets the object's velocity and angularvelocity to the current values of the controller, 
					// which simulates the forces of a throw.
					if (controllerOrigin != null) {
						
						rigidbody.velocity = controllerOrigin.TransformVector(device.velocity);
						rigidbody.angularVelocity = controllerOrigin.TransformVector(device.angularVelocity);
					}
					else {

						rigidbody.velocity = device.velocity;
						rigidbody.angularVelocity = device.angularVelocity;
					}

					rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;

					ControllerComponent.attachedObject = null;
					// Debug.Log("Object " + ControllerComponent.attachedObject.name +  "released");
				}
			});
		}
	}
}