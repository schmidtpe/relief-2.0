using UnityEngine;

public class SphereDeactEvent : EgoEvent {

	public readonly Color lastCubeColor;

	public SphereDeactEvent(Color lastCubeColor) {

		this.lastCubeColor = lastCubeColor;
	}
}
