
public class ColorBlinkEvent : EgoEvent {
	
	public readonly int color;

	public ColorBlinkEvent(int c) {

		this.color = c;
	}
}
