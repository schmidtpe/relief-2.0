using UnityEngine;

public class StartRotationEvent : EgoEvent {

	public readonly GameObject controlModul;

	public StartRotationEvent(GameObject controlModul) {

		this.controlModul = controlModul;
		
	}
}
