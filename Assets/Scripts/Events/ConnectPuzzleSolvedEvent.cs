
public class ConnectPuzzleSolvedEvent : EgoEvent {

	public readonly int index;

	public ConnectPuzzleSolvedEvent(int i) {

		this.index = i;
	}
}
