using UnityEngine;

public class PatternPuzzleObjHitEvent : EgoEvent {
	
	public readonly EgoComponent egoComponent;

	public PatternPuzzleObjHitEvent(EgoComponent e) {
		
		this.egoComponent = e;
	}
}
