using UnityEngine;

public class SphereActEvent : EgoEvent {
	
	public readonly Color cubeColor;

	public SphereActEvent(Color color) {
		
		this.cubeColor = color;
	}
}
