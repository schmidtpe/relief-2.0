using UnityEngine;

public class StopRotationEvent : EgoEvent {
	
	public readonly GameObject controlModul;

	public StopRotationEvent(GameObject controlModul) {

		this.controlModul = controlModul;
	}
}
