﻿
public class InputEvent : EgoEvent {

	public readonly string inputButton;

	public InputEvent(string inputButton) {

		this.inputButton = inputButton;
	}
}
