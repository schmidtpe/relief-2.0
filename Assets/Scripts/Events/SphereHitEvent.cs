using UnityEngine;

public class SphereHitEvent : EgoEvent {
	
	public readonly GameObject hittedSphere;

	public SphereHitEvent(GameObject hittedSphere) {

		this.hittedSphere = hittedSphere;
		
	}
}
