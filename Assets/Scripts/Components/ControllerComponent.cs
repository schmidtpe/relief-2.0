using UnityEngine;

[DisallowMultipleComponent]

public class ControllerComponent : MonoBehaviour {

    public Rigidbody attachPoint;
    public GameObject attachedObject;
    public SteamVR_Controller.Device device;   
}
