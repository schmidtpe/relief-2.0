using UnityEngine;

[DisallowMultipleComponent]

public class ConnectPuzzleSphereComponent : MonoBehaviour {

    public bool rotateLeft;
    public bool rotateRight;
    public bool isActive;
    public bool isSolved;
    public GameObject objectOnModul;
}