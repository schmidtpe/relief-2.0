using UnityEngine;

[DisallowMultipleComponent]

public class DrawLineDataComponent : MonoBehaviour {
	
    public float defDrawLineSpeed = 5.0f;  
	public float drawCounter;
    public float deleteCounter;
    public float defDist = 30.0f;
    public Vector3 defStartPoint = new Vector3(0,0,0); 	
    public Vector3 defEndPoint = new Vector3(30,0,0); 
    public string color; 
    public bool drawLine;
    public bool deleteLine;
    public bool isActive; 
    public bool solved;

}

