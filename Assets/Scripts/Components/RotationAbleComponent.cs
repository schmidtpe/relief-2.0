﻿using UnityEngine;

[DisallowMultipleComponent]

public class RotationAbleComponent: MonoBehaviour {

	public float rotateAroundSpeed;
	public Vector3 selfRotationSpeed;
	public Vector3 spinAxis;
}
