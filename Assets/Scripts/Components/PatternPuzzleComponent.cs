using UnityEngine;
using System.Collections.Generic;
using System.Threading;

[DisallowMultipleComponent]

public class PatternPuzzleComponent : MonoBehaviour {
	
    public int onList = 0;
	public List<int> pattern = new List<int>();
	public bool patternIsPlaying = false;
	public Thread patternThread;
	public bool threadStarted = false;
	public EgoComponent hittedEgoComponent;
}

