using UnityEngine;

[DisallowMultipleComponent]

public class CanCatchComponent : MonoBehaviour {
	
    public bool canCatch;
}
